package or.example.piseth_phoneshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ProblemDetail HandleNotFoundException (NotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle("Not Found!!!");
        problemDetail.setProperty("time", LocalDateTime.now());
        return problemDetail;
    }

    @ExceptionHandler(NotHaveDataException.class)
    @ResponseStatus(HttpStatus.OK)
    ProblemDetail HandleNotHaveDataException (NotHaveDataException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.OK, e.getMessage());
        problemDetail.setTitle("Not Have Data!!!");
        problemDetail.setProperty("time", LocalDateTime.now());
        return problemDetail;
    }

    @ExceptionHandler(InternalException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ProblemDetail HandleInternalException (InternalException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        problemDetail.setTitle("Not Data Match!!!");
        problemDetail.setProperty("time", LocalDateTime.now());
        return problemDetail;
    }

    @ExceptionHandler(BadRequestExceptions.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ProblemDetail HandleBadRequestException (BadRequestExceptions e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle("Bad Request!!!");
        problemDetail.setProperty("time", LocalDateTime.now());
        return problemDetail;
    }

}
