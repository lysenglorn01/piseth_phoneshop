package or.example.piseth_phoneshop.exception;

public class NotHaveDataException extends RuntimeException{

    public NotHaveDataException(String message) {
        super(message);
    }
}
