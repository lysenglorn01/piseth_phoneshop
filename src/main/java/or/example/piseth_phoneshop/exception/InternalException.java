package or.example.piseth_phoneshop.exception;

public class InternalException extends RuntimeException{
    public InternalException(String message) {
        super(message);
    }
}
