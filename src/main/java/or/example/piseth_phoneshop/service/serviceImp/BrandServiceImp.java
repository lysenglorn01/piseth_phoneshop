package or.example.piseth_phoneshop.service.serviceImp;

import or.example.piseth_phoneshop.dto.BrandDTO;
import or.example.piseth_phoneshop.entity.Brand;
import or.example.piseth_phoneshop.exception.BadRequestExceptions;
import or.example.piseth_phoneshop.exception.InternalException;
import or.example.piseth_phoneshop.exception.NotFoundException;
import or.example.piseth_phoneshop.exception.NotHaveDataException;
import or.example.piseth_phoneshop.mapper.BrandMapper;
import or.example.piseth_phoneshop.repository.BrandRepository;
import or.example.piseth_phoneshop.request.BrandRequest;
import or.example.piseth_phoneshop.service.BrandService;
import or.example.piseth_phoneshop.specification.BrandSpecification;
import org.apache.coyote.BadRequestException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("BrandService")
public class BrandServiceImp implements BrandService {
    private final BrandRepository brandRepository;
    private final BrandMapper brandMapper;

    public BrandServiceImp(BrandRepository brandRepository, BrandMapper brandMapper) {
        this.brandRepository = brandRepository;
        this.brandMapper = brandMapper;
    }

    @Override
    public BrandDTO insertBrand(BrandRequest brandRequest) {
        Brand brand = brandMapper.brandToBrandRequest(brandRequest);
        Brand insertBrand = brandRepository.save(brand);

        return brandMapper.brandToBrandDTO(insertBrand);
    }

    @Override
    public List<BrandDTO> getAllBrand() {
        List<Brand> brands = brandRepository.findAll();

        if (brands.isEmpty()) {
            throw new NotHaveDataException("No brands found in the database.");
        }

        List<BrandDTO> brandDTOs = brandMapper.listBrandToBrandDTOs(brands);
        return brandDTOs;
    }

    @Override
    public List<BrandDTO> findBrandByName(String brandName) {
        List<Brand> brands = brandRepository.findByBrandNameContainingIgnoreCase(brandName);
        if (brands.isEmpty()) {
            throw new NotHaveDataException("No brands found with the name: " + brandName);
        }
        return brandMapper.listBrandToBrandDTOs(brands);
    }

    @Override
    public BrandDTO getBrandById(Integer id) {
        return brandRepository.findById(id).map(brandMapper::brandToBrandDTO).orElseThrow(() -> new NotFoundException("Brand not found with a ID " + id));
    }

    @Override
    public BrandDTO updateBrand(Integer id, BrandRequest brandRequest) {
        Brand brand = brandRepository.findById(id).orElseThrow(() -> new InternalException("Brand not found with ID " + id));

        // Use the mapper to update the brand entity with values from the brand request.
        brandMapper.updateBrandToBrandRequest(brandRequest, brand);

        // Save the updated brand entity.
        Brand updatedBrand = brandRepository.save(brand);

        // Convert the updated brand entity to a DTO.
        return brandMapper.brandToBrandDTO(updatedBrand);
    }

    @Override
    public BrandDTO deleteBrand(Integer id) {
        Brand brand = brandRepository.findById(id).orElseThrow(() -> new InternalException("Brand not found with ID " + id));
        BrandDTO brandDTO = brandMapper.brandToBrandDTO(brand);
        brandRepository.deleteById(id);
        return brandDTO;
    }

    @Override
    public List<BrandDTO> searchBrands(String brandName, String description) {
        if ((brandName == null || brandName.trim().isEmpty()) && (description == null || description.trim().isEmpty())) {
            throw new BadRequestExceptions("At least one search criteria must be provided.");
        }

        try {
            List<Brand> brands = brandRepository.findAll(
                    Specification.where(BrandSpecification.hasName(brandName))
                            .and(BrandSpecification.hasDescription(description))
            );

            if (brands.isEmpty()) {
                throw new NotFoundException("No brands found matching the criteria.");
            }

            return brandMapper.listBrandToBrandDTOs(brands);
        } catch (Exception ex) {
            throw new RuntimeException("Error while searching brands",ex);
        }
    }
}
