package or.example.piseth_phoneshop.service;

import or.example.piseth_phoneshop.dto.BrandDTO;
import or.example.piseth_phoneshop.request.BrandRequest;
import org.apache.coyote.BadRequestException;

import java.util.List;


public interface BrandService {
    List<BrandDTO> getAllBrand();
    List<BrandDTO> findBrandByName(String brandName);
    BrandDTO getBrandById(Integer id);
    BrandDTO insertBrand(BrandRequest brandRequest);
    BrandDTO updateBrand(Integer id, BrandRequest brandRequest);
    BrandDTO deleteBrand(Integer id);
    List<BrandDTO> searchBrands(String brandName, String description);
}
