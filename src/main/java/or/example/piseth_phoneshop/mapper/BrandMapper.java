package or.example.piseth_phoneshop.mapper;

import or.example.piseth_phoneshop.dto.BrandDTO;
import or.example.piseth_phoneshop.entity.Brand;
import or.example.piseth_phoneshop.request.BrandRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BrandMapper {
    BrandMapper INSTANCE = Mappers.getMapper(BrandMapper.class);
    Brand brandToBrandRequest(BrandRequest brandRequest);
    BrandDTO brandToBrandDTO(Brand brand);
    List<BrandDTO> listBrandToBrandDTOs(List<Brand> brand);
    void updateBrandToBrandRequest(BrandRequest brandRequest, @MappingTarget Brand brand);
}
