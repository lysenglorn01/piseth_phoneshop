package or.example.piseth_phoneshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PisethPhoneShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(PisethPhoneShopApplication.class, args);
	}

}
