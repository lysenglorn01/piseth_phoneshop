package or.example.piseth_phoneshop.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "brands")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brand_id", nullable = false)
    private Integer brandId;
    @Column(name = "brand_name", length = 20)
    private String brandName;
    @Column(name = "descri", length = 50)
    private String description;
}
