package or.example.piseth_phoneshop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "models")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id", nullable = false)
    private Integer modelId;
    @Column(name = "model_name", length = 100)
    private String modelName;
    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;
}
