package or.example.piseth_phoneshop.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BrandDTO {
    private String brandName;
    private String description;
}
