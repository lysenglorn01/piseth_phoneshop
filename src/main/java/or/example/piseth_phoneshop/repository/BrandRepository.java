package or.example.piseth_phoneshop.repository;

import or.example.piseth_phoneshop.dto.BrandDTO;
import or.example.piseth_phoneshop.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface BrandRepository extends JpaRepository<Brand, Integer>, JpaSpecificationExecutor<Brand> {
    List<Brand> findByBrandNameContainingIgnoreCase(String brandName);
}
