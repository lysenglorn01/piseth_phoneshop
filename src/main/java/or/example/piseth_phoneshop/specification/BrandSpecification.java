package or.example.piseth_phoneshop.specification;

import or.example.piseth_phoneshop.entity.Brand;
import org.springframework.data.jpa.domain.Specification;

public class BrandSpecification {

    public static Specification<Brand> hasName(String brandName) {
        return (root, query, criteriaBuilder) ->
                brandName == null ? criteriaBuilder.conjunction() :
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("brandName")), "%" + brandName.toLowerCase() + "%");
    }

    public static Specification<Brand> hasDescription(String description) {
        return (root, query, criteriaBuilder) ->
                description == null ? criteriaBuilder.conjunction() :
                        criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + description.toLowerCase() + "%");
    }
}
