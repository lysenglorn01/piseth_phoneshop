package or.example.piseth_phoneshop.controller;

import io.swagger.v3.oas.annotations.Operation;
import or.example.piseth_phoneshop.dto.BrandDTO;
import or.example.piseth_phoneshop.entity.Brand;
import or.example.piseth_phoneshop.request.BrandRequest;
import or.example.piseth_phoneshop.response.ApiBaseResponse;
import or.example.piseth_phoneshop.service.BrandService;
import org.apache.coyote.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/brand")
public class BrandController {
    private final BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @PostMapping("/create")
    @Operation(summary = "create a new brand with brand request")
    public ResponseEntity<?> createBrand(@RequestBody BrandRequest brandRequest) {
        BrandDTO dataBrandDTO = brandService.insertBrand(brandRequest);
        ApiBaseResponse<BrandDTO> response = ApiBaseResponse.<BrandDTO>builder()
                .time(LocalDateTime.now())
                .message("Successful insert new brand!!")
                .status(HttpStatus.OK)
                .data(dataBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/list")
    @Operation(summary = "Get all Brands")
    public ResponseEntity<?> getAllBrand() {
        List<BrandDTO> dataBrandDTO = brandService.getAllBrand();
        ApiBaseResponse<List<BrandDTO>> response = ApiBaseResponse.<List<BrandDTO>>builder()
                .time(LocalDateTime.now())
                .message("Successful fetch all brand")
                .status(HttpStatus.OK)
                .data(dataBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Brand by ID")
    public ResponseEntity<?> getBrandByID(@PathVariable Integer id) {
        BrandDTO dataBrandDTO = brandService.getBrandById(id);
        ApiBaseResponse<BrandDTO> response = ApiBaseResponse.<BrandDTO>builder()
                .time(LocalDateTime.now())
                .message("Successful fetch data")
                .status(HttpStatus.OK)
                .data(dataBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/filter")
    @Operation(summary = "Filter Brand by name")
    public ResponseEntity<ApiBaseResponse<List<BrandDTO>>> filterBrandByName(@RequestParam("brandName") String brandName) {
        List<BrandDTO> filteredBrandDTO = brandService.findBrandByName(brandName);
        ApiBaseResponse<List<BrandDTO>> response = ApiBaseResponse.<List<BrandDTO>>builder()
                .time(LocalDateTime.now())
                .message("Successful filtered Brands by name")
                .status(HttpStatus.OK)
                .data(filteredBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/search")
    @Operation(summary = "Dynamic brands search")
    public ResponseEntity<ApiBaseResponse<List<BrandDTO>>> searchProducts(
            @RequestParam(required = false) String brandName,
            @RequestParam(required = false) String description) throws BadRequestException {
        List<BrandDTO> brand = brandService.searchBrands(brandName, description);
        ApiBaseResponse<List<BrandDTO>> response = ApiBaseResponse.<List<BrandDTO>>builder()
                .time(LocalDateTime.now())
                .message("Successful filtered!!!")
                .status(HttpStatus.OK)
                .data(brand)
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update new brand")
    public ResponseEntity<?> updateBrand(@PathVariable Integer id, @RequestBody BrandRequest brandRequest) {
        BrandDTO dataBrandDTO = brandService.updateBrand(id, brandRequest);
        ApiBaseResponse<BrandDTO> response = ApiBaseResponse.<BrandDTO>builder()
                .time(LocalDateTime.now())
                .message("Successful update new brand!!")
                .status(HttpStatus.OK)
                .data(dataBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBrand(@PathVariable Integer id) {
        BrandDTO dataBrandDTO = brandService.deleteBrand(id);
        ApiBaseResponse<BrandDTO> response = ApiBaseResponse.<BrandDTO>builder()
                .time(LocalDateTime.now())
                .message("Successful delete brand!!")
                .status(HttpStatus.OK)
                .data(dataBrandDTO)
                .build();
        return ResponseEntity.ok(response);
    }
}
